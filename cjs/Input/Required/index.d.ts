import { ErrorFenextjs } from "../../Fenextjs";
export declare class ErrorInputRequired extends ErrorFenextjs {
    constructor(d?: {
        input?: string;
    });
}
